from sqlalchemy import create_engine
from sqlalchemy.orm import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, Integer, String,Table,ForeignKey, DateTime
from sqlalchemy.orm import relationship

from datetime import datetime
import re

engine = create_engine('sqlite:///test.db', echo = True)

Base = declarative_base()

Session = sessionmaker(bind=engine)
session = Session()

post_tags = Table(
   'post_tags', Base.metadata,
   Column('tag_id', Integer, ForeignKey('posts.id')), 
   Column('post_id', Integer, ForeignKey('tags.id')) 
)

def slugify(string):
		pattern = r'[^\w+]'
		return re.sub(pattern, '-', string)

class Post(Base):
    __tablename__ = 'posts'

    id = Column(Integer, primary_key=True)
    slug = Column(String, unique=True)
    title = Column(String)
    body = Column(String)
    created = Column(DateTime, default=datetime.utcnow)
    tags = relationship("Tag",
                secondary=post_tags,
                backref="posts")

    def __init__(self, *args, **kwargs):
        super(Post, self).__init__(*args, **kwargs)
        self.slug = slugify(self.title)

    def __repr__(self):
		    return f'<Post id {self.id} title: {self.title} slug: {self.slug} >'


class Tag(Base):
    __tablename__ = 'tags'

    id = Column(Integer, primary_key=True)
    slug = Column(String, unique=True)
    name = Column(String)

    def __init__(self, *args, **kwargs):
        super(Tag, self).__init__(*args, **kwargs)
        self.slug = slugify(self.name)

    def __repr__(self):
        return f'Tag: {self.name}  slug: {self.slug} '



Base.metadata.create_all(engine)

