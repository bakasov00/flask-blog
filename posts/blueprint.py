from flask import Blueprint, render_template, request, redirect, url_for
from models import Post, Tag, session
from .forms import PostForm

posts = Blueprint('posts', __name__, template_folder='templates')

@posts.route('/create', methods=['POST', 'GET'])
def create_post():
    if request.method == 'POST':
        title = request.form.get('title')
        body = request.form.get('body')
        try:
            post = Post(title=title, body=body)
            session.add(post)
            session.commit()
        except:
            print('Error')
        return redirect(url_for('posts.index'))

    form = PostForm()
    return render_template('posts/create_post.html', form=form)


@posts.route('/edit/<slug>', methods=['POST', 'GET'])
def edit_post(slug):
    post = session.query(Post).filter(Post.slug == slug).first()
    if request.method == 'POST':
        form = PostForm(formdata=request.form, obj=post)
        form.populate_obj(post)
        session.commit()

        return redirect(url_for('posts.detail', slug=post.slug))
    
    form = PostForm(obj=post)
    return render_template('posts/edit.html', form=form, post=post)

@posts.route('/')
def index():
    q = request.args.get('q')
    if q:
        all_post = session.query(Post).filter( Post.title.contains(q) | Post.body.contains(q) ).all()
    else:
        all_post = session.query(Post).all()
    return render_template('posts/index.html', all_post=all_post)


@posts.route('/<slug>')
def detail(slug):
    post = session.query(Post).filter(Post.slug == slug).first()
    return render_template('posts/detail.html', post=post)

@posts.route('/tag/<slug>')
def tag(slug):
    tag = session.query(Tag).filter(Tag.slug == slug).first()
    return render_template('posts/tag.html', tag=tag)
