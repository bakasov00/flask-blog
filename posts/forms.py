from wtforms import TextField, TextAreaField, Form


class PostForm(Form):
    title = TextField('Title')
    body = TextAreaField('Body')