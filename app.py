from flask import Flask
from models import Post, session, Tag
from posts.blueprint import posts

app = Flask(__name__)
app.register_blueprint(posts, url_prefix='/blog') 

app.config.from_pyfile('yourconfig.cfg')


###### Admin Panel ######
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView

admin = Admin(app)
admin.add_view(ModelView(Tag, session))
admin.add_view(ModelView(Post, session))
